package model;

import java.util.Arrays;

public class PlayingBoard {
    public static final int DEFAULT_BOARD_SIZE = 3;
    public static final char DEFAULT_SIGN_IN_BOARD = '.';

    private char[][] board;

    public PlayingBoard() {
        board = new char[DEFAULT_BOARD_SIZE][DEFAULT_BOARD_SIZE];
        fillBoard();
    }

    private void fillBoard() {
        for (char[] cells : board) {
            Arrays.fill(cells, DEFAULT_SIGN_IN_BOARD);
        }
    }

    public void changeTheSingInTheCell(char sign, int x, int y){
        board[y][x] = sign;
    }

    public char[][] getBoard() {
        return board;
    }

    public void setBoard(char[][] board) {
        this.board = board;
    }

    @Override
    public String toString() {
        StringBuilder stringBoard = new StringBuilder();
        stringBoard.append("\n  1 2 3 x \n");
        for (int i = 0; i < board.length; i++) {
            stringBoard.append(i + 1).append(" ");
            for (char cell : board[i]) {
                stringBoard.append(cell).append(" ");
            }
            stringBoard.append("\n");
        }
        stringBoard.append("y\n");
        return stringBoard.toString();
    }
}
