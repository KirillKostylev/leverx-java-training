package model;

import java.util.Random;

import static model.PlayingBoard.DEFAULT_BOARD_SIZE;

public class AIPlayer extends Player {
    private Random random = new Random();

    public AIPlayer(String name, char sign) {
        super(name, sign);
    }


    @Override
    public void takeStep(PlayingBoard board) {
        int x;
        int y;
        while (true) {
            x = random.nextInt(DEFAULT_BOARD_SIZE);
            y = random.nextInt(DEFAULT_BOARD_SIZE);
            if (cellIsFree(board, x, y)) {
                break;
            }
        }
        board.changeTheSingInTheCell(getSign(), x, y);
    }
}
