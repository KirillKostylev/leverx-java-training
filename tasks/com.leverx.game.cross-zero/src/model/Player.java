package model;

public abstract class Player {
    private String name;
    private char sign;

    public abstract void takeStep(PlayingBoard board);

    public boolean checkForWin(PlayingBoard playingBoard) {
        char[][] board = playingBoard.getBoard();

        for (int i = 0; i < board.length; i++) {
            if (board[0][i] == sign && board[1][i] == sign && board[2][i] == sign ||
                    board[i][0] == sign && board[i][1] == sign && board[i][2] == sign) {
                return true;
            }
        }
        return board[0][0] == sign && board[1][1] == sign && board[2][2] == sign ||
                board[2][0] == sign && board[1][1] == sign && board[0][2] == sign;
    }

    public boolean checkForFullBoard(PlayingBoard playingBoard) {
        for (char[] cells : playingBoard.getBoard()) {
            for (char cell : cells) {
                if (cell == PlayingBoard.DEFAULT_SIGN_IN_BOARD) {
                    return false;
                }
            }
        }
        return true;
    }

    public boolean cellIsFree(PlayingBoard playingBoard, int x, int y) {
        char[][] board = playingBoard.getBoard();
        return board[y][x] == PlayingBoard.DEFAULT_SIGN_IN_BOARD;
    }

    public Player(String name, char sign) {
        this.name = name;
        this.sign = sign;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public char getSign() {
        return sign;
    }

    public void setSign(char sign) {
        this.sign = sign;
    }
}
