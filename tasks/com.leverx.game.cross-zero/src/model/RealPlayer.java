package model;


import static util.Utils.inputIntWithLimits;

public class RealPlayer extends Player {


    public RealPlayer(String name, char sign) {
        super(name, sign);
    }

    @Override
    public void takeStep(PlayingBoard playingBoard) {
        int x = 0;
        int y = 0;
        while (true) {
            x = inputIntWithLimits(1, 3, "x = ") - 1;
            y = inputIntWithLimits(1, 3, "y = ") - 1;
            if (cellIsFree(playingBoard, x, y)) {
                break;
            }
            System.out.println("This cell is already busy, repeat the step.");
        }
        playingBoard.changeTheSingInTheCell(getSign(), x, y);
    }

}
