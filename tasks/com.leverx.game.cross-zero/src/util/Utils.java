package util;

import java.util.Scanner;

public class Utils {
    public static final Scanner SCANNER = new Scanner(System.in);

    public static int inputInt(String msg) {
        while (true) {
            System.out.print(msg);
            if (SCANNER.hasNextInt()) {
                return SCANNER.nextInt();
            }
            System.out.println("\nIncorrect data format, try again");
            SCANNER.nextLine();
        }
    }

    public static int inputIntWithLimits(int min, int max, String msg) {
        while (true) {
            int i = inputInt(msg);
            if (i >= min && i <= max) {
                return i;
            }
            System.out.println("Incorrect value, it have to be from " + min + " to " + max);
        }
    }

}
