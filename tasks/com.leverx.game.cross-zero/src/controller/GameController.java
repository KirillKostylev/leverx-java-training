package controller;

import model.AIPlayer;
import model.Player;
import model.PlayingBoard;
import model.RealPlayer;
import util.Utils;

public class GameController {
    public static final char SIGN_X = 'X';
    public static final char SIGN_O = 'O';
    private static final String NOBODY_WON_MESSAGE = "Nobody won in this game.";
    private static final String START_MENU_MESSAGE = "Select the game mode:\n\t1 - Play with person\n\t2 - Play with AI\nGame mode: ";

    private boolean gameIsRun = true;

    public void startGame() {

        int gameMode = Utils.inputIntWithLimits(1, 2,
                START_MENU_MESSAGE);


        PlayingBoard board = new PlayingBoard();
        Player player1 = new RealPlayer("Player 1", SIGN_X);
        Player player2;

        switch (gameMode) {
            case 1:
                player2 = new RealPlayer("Player 2", SIGN_O);
                break;
            case 2:
            default:
                player2 = new AIPlayer("Computer", SIGN_O);
        }


        while (gameIsRun) {
            step(player1, board);
            if (!gameIsRun) {
                break;
            }
            step(player2, board);
        }

    }

    private void step(Player player, PlayingBoard board) {
        System.out.println(board.toString());
        System.out.println(player.getName() + "'s turn: ");

        player.takeStep(board);

        boolean isWin = player.checkForWin(board);
        if (isWin) {
            finishGame();
            System.out.println(board.toString());
            System.out.println(player.getName() + " wins!!! ");
            return;
        }

        boolean boardIsFull = player.checkForFullBoard(board);
        if (boardIsFull) {
            finishGame();
            System.out.println(board.toString());
            System.out.println(NOBODY_WON_MESSAGE);
        }
    }

    private void finishGame() {
        gameIsRun = false;
    }


}
